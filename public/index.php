<?php

require dirname(__DIR__).'/vendor/autoload.php';

// Let php-server handle js/css files
$extensions = array("js", "css", "png");
$path = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
$ext = pathinfo($path, PATHINFO_EXTENSION);
if (in_array($ext, $extensions)) return false;

// We'll use these here
use Finwo\ChatApp\Db;
use Finwo\ChatApp\Document\Account;
use Finwo\ChatApp\Document\Column;
use Finwo\ChatApp\Route;

// Ensure database path
if (!isset($_ENV['DATABASE_URL'])) {
  $_ENV['DATABASE_URL'] = 'sqlite:chatapp.db';
}

// Attempt fetching account 1
$basedata = require dirname(__DIR__).'/basedata.php';
$pdo      = Db::pdo();
$db       = Db::instance();
$driver   = $pdo->getAttribute(\PDO::ATTR_DRIVER_NAME);

// Create tables if not existing yet
foreach ($basedata['schema'] as $tableName => $columns) {
  $sql         = "CREATE TABLE IF NOT EXISTS `${tableName}` (";
  $firstColumn = true;
  foreach ( $columns as $columnName => $columnDefinition ) {
    $columnData       = json_decode(json_encode($columnDefinition));
    $columnData->name = $columnName;
    $column           = Db::mapper()->map($columnData,new Column());

    // Add the separator
    if ($firstColumn) {
      $firstColumn = false;
    } else {
      $sql .= ', ';
    }

    $sql .= $column->toSql($driver);
  }
  $sql .= ');';
  $pdo->exec($sql);
}

// Insert basedata
foreach($basedata['data'] as $tableName => $rows) {
  $table = $db->table($tableName);
  $found = $table->fetch();
  if (is_null($found)) {
    $result = $table->insert($rows);
  }
}

// Load routes
$files = glob(dirname(__DIR__).'/route/*.php');
sort($files);
foreach( $files as $filename ) {
  include $filename;
}

// Trigger routing
Route::instance()->dispatch();

// Delete expired sessions
$sessions = $db
  ->table('session')
  ->where('`expire` < ' . time())
  ->fetchAll();
foreach( $sessions as $session ) {
  $session->delete();
}
