<!DOCTYPE html>
<html>
  <head>
    <title><?= $this->e($title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- <link rel="stylesheet" href="https://unpkg.com/picnic"> -->
    <link rel="stylesheet" href="https://unpkg.com/picnic@6.5.1/picnic.min.css">
    <link rel="stylesheet" href="/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rivets/0.9.6/rivets.bundled.min.js"></script>
  </head>
  <body>
    <nav>
      <a href="/" class="brand">
        <img class="logo" src="/logo.png"/>
        <span>Chat app</span>
      </a>

      <input id="bmenub" type="checkbox" class="show">
      <label for="bmenub" class="burger pseudo button">&#9776;</label>

      <div class="menu">
        <?php if ($authenticated) { ?>
          <a href="/logout" class="pseudo button">Log out</a>
        <?php } ?>
      </div>

    </nav>
    <main>
      <?= $this->section('content') ?>
    </main>
  </body>
</html>
