<?php $this->layout('layout',['title'=>'Home','authenticated'=>true]); ?>

<div class="card">
  <code rv-each-message="messages" rv-class-private="message.to">
    <a rv-class-me="message.fromMe">{message.from}</a><a rv-if="message.to"> &gt; <a rv-class-me="message.toMe">{message.to}</a></a>: {message.data}
    <div class="right">{message.date | date}</div>
  </code>
</div>

<form onsubmit="return app.action.send(this)">
  <input name="message" autocomplete="off" autofocus />
  <button class="right">Send</button>
</form>

<script>

  // Store data here
  const app = {
    username: '<?= $account->username ?>',
    messages: [],
    lastMessage: false,

    // Removes duppicates from an array
    // Uses a function to generate a key for each entry
    removeDuplicatesBy(keyFn, array) {
      const mySet = new Set();
      return array.filter(function(x) {
        const key = keyFn(x), isNew = !mySet.has(key);
        if (isNew) mySet.add(key);
        return isNew;
      });
    },

    // xhr requests
    xhr( method, url, data, callback ) {
      callback  = callback || function(){};
      method    = method.toUpperCase();
      const xhr = new XMLHttpRequest();
      xhr.open(method, url);
      xhr.onreadystatechange = function () {
          if (xhr.readyState == 4 && xhr.status == 200) {
              try {
                callback(JSON.parse(xhr.responseText));
              } catch(e) {
                console.log('ERR', e);
                callback(xhr.responseText);
              }
          }
      }
      switch(method) {
        case 'GET':
          xhr.send();
          break;
        case 'POST':
          xhr.setRequestHeader('content-type', 'application/json');
          xhr.send(JSON.stringify(data));
          break;
        default:
          return;
      }
    },

    command: {

      // Send a private message
      msg( fullText ) {
        const tokens = fullText.split(' ');
        const to     = tokens.shift();
        const data   = tokens.join(' ');
        app.xhr('POST', '/message', {data,to}, app.action.receive);
      },

    },

    // Commands
    action: {

      // Text input receiver
      // Unless a command was given, sends a public message
      send( formElement ) {
        let data = formElement.message.value;
        formElement.message.value = '';

        if (data.substr(0,1) === '/') {
          const tokens  = data.substr(1).split(' ');
          const command = tokens.shift();
          data          = tokens.join(' ');
          if (!(command in app.command)) return false;
          app.command[command](data);
          return false;
        }

        app.xhr('POST', '/message', {data}, app.action.receive);
        return false;
      },

      // Polling receiver
      receive() {

        // Define the GET query to use
        let query = '';
        if (app.lastMessage) {
          query = '?since=' + (app.lastMessage-60);
        }

        // Start fetching
        app.xhr('GET', '/message' + query, null, function(response) {
          if (!response.success) return;
          response.data.forEach(function(message) {

            // Convert timestamps into milliseconds
            message.when = parseInt(message.when) * 1000;

            // Detect private message direction
            message.fromMe = message.from === app.username;
            message.toMe   = message.to   === app.username;

            // Add to the list and prevent duplicates
            app.messages.push(message);
            app.messages = app.removeDuplicatesBy( obj=>obj.id, app.messages );

            // Sort and limit the view
            app.messages.sort(function(left, right) {
              return left.when - right.when;
            });
            while(app.messages.length > 20) {
              app.messages.shift();
            }
          });
        });
      },
    }
  };

  // Attach data
  rivets.bind(document.body, app);

  // Set up the actual polling
  app.action.receive();
  setInterval(app.action.receive, 5000);
</script>
