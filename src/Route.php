<?php

namespace Finwo\ChatApp;

use Klein\Klein;

class Route extends Klein {

  /**
   * Create a router instance
   *
   * Returns the same instance on consecutive calls
   *
   * @return  Klein
   */
  public static function instance() {
    static $router = null;

    if (is_null($router)) {
      $router = new Route();
    }

    return $router;
  }

}
