<?php

namespace Finwo\ChatApp\Document;

use Finwo\ChatApp\Db;

abstract class AbstractDocument {

  /**
   * @var int
   */
  public $id = null;

  /**
   * Document builder, allows inserting data
   *
   * @param  array|null  $data  Data to insert into the document
   */
  public function __construct( $data = null ) {
    if (!is_null($data)) {
      if (is_array($data)) {
        // TODO: properly map into an stdObject
        $data = json_decode(json_encode($data));
      }
      Db::mapper()->map($data, $this );
    }
  }

  /**
   * Detect the table a document belongs to
   *
   * @return  string
   */
  public static function table() {
    $class = get_called_class();
    $short = @array_pop(explode('\\',$class));
    return strtolower($short);
  }

  /**
   * Fetch a single document
   *
   * @param  integer  $id  ID of the document to fetch
   *
   * @return  AbstractDocument|null
   */
  public static function get( $id ) {
    try {
      $class = get_called_class();
      $db    = Db::instance();
      $row   = $db->table( self::table(), intval($id) );
      return new $class($row);
    } catch(\Exception $e) {
      return null;
    }
  }

  /**
   * Find a single document
   *
   * @param  string  $column  The column/field to search on
   * @param  mixed   $value   The value to match the column/field with
   *
   * @return  AbstractDocument|null
   */
  public static function findOne( $column, $value ) {
    $class = get_called_class();
    $db    = Db::instance();
    $table = $db->table( self::table() );
    $row   = $table->where( $column, $value )->fetch();

    if (is_null($row) || (!$row->exists())) {
      return null;
    }

    return new $class($row);
  }

  /**
   * Start a query on the document's table
   *
   * @return  \LessQL\Database\Result|\LessQL\Database\Row|null
   */
  public static function query() {
    return Db::instance()->table(self::table());
  }

  /**
   * Save the current document
   *
   * @return  AbstractDocument
   */
  public function save() {
    $db = Db::instance();

    // New document
    if (is_null($this->id)) {

      // Prepare data
      $bare = get_object_vars($this);
      unset($bare['id']);

      // Insert into table
      $row = $db->createRow( self::table(), $bare );
      $row->save();

      // Map back auto-filled fields
      Db::mapper()->map($row, $this);

      return $this;
    }

    // Existing document

    // Prepare data
    $bare = get_object_vars($this);

    // Fetch original and save new data
    $row  = $db->table( self::table(), $this->id );
    $row->update($bare);

    // Map back auto-filled fields
    Db::mapper()->map($row, $this);

    return $this;
  }

  /**
   * Delete this document
   *
   * @return  AbstractDocument
   */
  public function delete() {
    self::query()
      ->where('id', $this->id)
      ->delete();
    $this->id = null;
    return $this;
  }
}
