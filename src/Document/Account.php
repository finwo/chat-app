<?php

namespace Finwo\ChatApp\Document;

class Account extends AbstractDocument {

  /**
   * @var string
   */
  public $username;

  /**
   * @var string
   */
  protected $passhash;

  /**
   * Check if a password matches or not
   *
   * @param  string  $password  The password to verify
   *
   * @return  boolean
   */
  public function verifyPassword( $password ) {
    return password_verify($password,$this->passhash);
  }

  /**
   * Set the password hash on the account
   *
   * @param  string  $passhash  The hash to insert into the document
   *
   * @return  Account
   */
  public function setPasshash( $passhash ) {
    $this->passhash = $passhash;
    return $this;
  }

  /**
   * Set a new password on the account
   *
   * Saves the updated password instantly to the database
   *
   * @param  string  $password  The new password to insert
   *
   * @return  Account
   */
  public function setPassword( $password ) {
    $this->passhash = password_hash( $password, PASSWORD_BCRYPT );
    $this->save();
    return $this;
  }

}
