<?php

namespace Finwo\ChatApp\Document;

class Message extends AbstractDocument {

  /**
   * @var string
   */
  public $from;

  /**
   * @var string|null
   */
  public $to;

  /**
   * @var int
   */
  public $when;

  /**
   * @var string
   */
  public $data;

  /**
   * Fetch messages for an account
   *
   * @param  string        $account  The username of the account to fetch messages for
   * @param  integer|null  $since    Timestamp (seconds) to fetch messages after
   * @param  integer       $limit    The maximum number of messages per subgroup
   *
   * @return  array
   */
  public static function fetch( $account, $since = null, $limit = 20 ) {
    $result = [];

    // Ensure since is an int
    // We're working before 1970, 0 is fine
    $since = intval($since);

    // Fetch public messages
    $fetched = self::query()
      ->where('to', null)
      ->where("`when` >= ${since}")
      ->orderBy('when', 'DESC')
      ->limit($limit)
      ->fetchAll() ?: [];
    $result = array_merge($result, $fetched);

    // Fetch private received messages
    $fetched = self::query()
      ->where('to', $account)
      ->where("`when` >= ${since}")
      ->orderBy('when', 'DESC')
      ->limit($limit)
      ->fetchAll() ?: [];
    $result = array_merge($result, $fetched);

    // Fetch private sent messages
    $fetched = self::query()
      ->whereNot('to', null)
      ->where('from', $account)
      ->where("`when` >= ${since}")
      ->orderBy('when', 'DESC')
      ->limit($limit)
      ->fetchAll() ?: [];
    $result = array_merge($result, $fetched);

    return $result;
  }

}
