<?php

namespace Finwo\ChatApp\Document;

class Column {
  public $name      = null;
  public $increment = false;
  public $primary   = false;
  public $unique    = false;
  public $type      = 'integer';
  public $null      = false;
  public $length    = 64;

  /**
   * Convert the internal column description into an SQL column description
   *
   * @param  string  $driver  Which driver is used by the PDO
   *
   * @return  string
   */
  public function toSql( $driver ) {
    $sql  = '`';
    $sql .= $this->name;
    $sql .= '`';

    // Set the type
    switch($this->type) {
      case 'bigint' : $sql .= ' BIGINT'; break;
      case 'integer': $sql .= ' INTEGER'; break;
      case 'string' : $sql .= ' VARCHAR(' . $this->length . ')'; break;
      case 'text'   : $sql .= ' TEXT'; break;
    }

    // Add primary-ness
    if ($this->primary) {
      $sql .= ' PRIMARY KEY';
    }

    // Handle differing auto-increment definitions
    if ($this->increment) {
      switch($driver) {
        case 'sqlite': $sql .= ' AUTOINCREMENT' ; break;
        case 'mysql' : $sql .= ' AUTO_INCREMENT'; break;
      }
    }

    // Whether or not null is allowed
    if ($this->null) {
      $sql .= ' NULL';
    } else {
      $sql .= ' NOT NULL';
    }

    return $sql;
  }
}
