<?php

namespace Finwo\ChatApp\Document;

use Finwo\ChatApp\Db;

class Session extends AbstractDocument {

  /**
   * @var string
   */
  public $account;

  /**
   * @var string
   */
  public $token;

  /**
   * @var string|null
   */
  public $expire;

  /**
   * @var string|null
   */
  public $data;

  /**
   * Session initializer
   *
   * Starts a session on creation of the document
   *
   * @param  array|null  $data  Data to insert into the outer session storage
   *
   * TODO: Make session creation optional
   */
  public function __construct( $data = null ) {

    // Generate random session id
    $alphabet       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $alphabetLength = strlen($alphabet);
    $this->token    = '';
    for ($i = 0; $i < 64; $i++) {
        $this->token .= $alphabet[rand(0, $alphabetLength - 1)];
    }

    // Inherit behavior
    parent::__construct( $data );

    // PHP knows the session
    session_start();
    session_id($this->token);

    // Load session data
    $loadedData = json_decode($data['data'], true);
    if (!is_array($loadedData)) $loadedData = [];
    array_replace($_SESSION,$loadedData);

    // (re)set expiry
    $this->expire = time() + 3600;

    // Save for re-use
    $this->save();

    // Save data on shutdown
    $session = $this;
    register_shutdown_function(function() use ($session) {
      if (is_null($session->id)) return;
      $session->data = json_encode($_SESSION);
      $session->save();
    });
  }
}
