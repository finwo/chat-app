<?php

namespace Finwo\ChatApp;

use \PDO;
use \JsonMapper;
use LessQL\Database;

class Db {

  /**
   * Connects to the database and returns the same instance on consecutive calls
   *
   * @return  \PDO
   */
  public static function pdo() {
    static $pdo = null;

    // Load db if not there yet
    if(is_null($pdo)) {
      $pdo = new \PDO($_ENV['DATABASE_URL'], $_ENV['DATABASE_USER'] ?: '', $_ENV['DATABASE_PASS'] ?: '');
    }

    return $pdo;
  }

  /**
   * Initializes the orm and returns the same instance on consecutive calls
   *
   * @return  Database
   */
  public static function instance() {
    static $db  = null;

    // Initialize orm
    if(is_null($db)) {
      $db = new Database(self::pdo());
    }

    return $db;
  }

  /**
   * Returns a basic field mapper, simplifying documents
   *
   * @return  JsonMapper
   */
  public static function mapper() {
    static $mapper = null;

    if(is_null($mapper)) {
      $mapper = new JsonMapper();
    }

    return $mapper;
  }
}
