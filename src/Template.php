<?php

namespace Finwo\ChatApp;

use League\Plates\Engine;

class Template {
  protected $name;
  protected $data = [];

  /**
   * Initializes the template engine
   *
   * Returns the same instance on consecutive calls
   *
   * @return  Engine
   */
  protected static function engine() {
    static $engine = null;
    if (is_null($engine)) {
      $engine = new Engine(dirname(__DIR__).'/template');
    }
    return $engine;
  }

  /**
   * Template initializer
   *
   * @param  string      $templateName  The name of the template to start loading
   * @param  array|null  $data          Data to insert into the template
   */
  public function __construct( $templateName, $data = null ) {
    $this->name = $templateName;

    if (is_array($data)) {
      $this->data = $data;
    }
  }

  /**
   * Render this template
   *
   * @param  array  $data  Data to insert into the template
   *
   * @return  string
   */
  public function render( $data = [] ) {
    return self::engine()->render($this->name, array_merge($this->data,$data));
  }

  /**
   * Turn this template into a string
   *
   * @return  string
   */
  public function __toString() {
    return $this->render();
  }
}
