<?php

return [
  'schema' => [
    'account' => [
      'id'       => ['primary'=>true,'increment'=>true],
      'username' => ['type'=>'string','unique'=>true],
      'passhash' => ['type'=>'string'],
    ],
    'message' => [
      'id'   => ['primary'=>true,'increment'=>true],
      'from' => ['type'=>'string'],
      'to'   => ['type'=>'string','null'=>true],
      'when' => ['type'=>'bigint'],
      'data' => ['type'=>'string','length'=>240],
    ],
    'session' => [
      'id'      => ['primary'=>true,'increment'=>true],
      'token'   => ['type'=>'string','unique'=>true],
      'account' => ['type'=>'string'],
      'expire'  => ['type'=>'integer'],
      'data'    => ['type'=>'text','null'=>true],
    ],
  ],
  'data' => [
    'account' => [
      ['id'=>1, 'username'=>'root' , 'passhash'=>password_hash('supersecret'  ,PASSWORD_BCRYPT)],
      ['id'=>2, 'username'=>'finwo', 'passhash'=>password_hash('pizza calzone',PASSWORD_BCRYPT)],
      ['id'=>3, 'username'=>'some1', 'passhash'=>password_hash('else'         ,PASSWORD_BCRYPT)],
    ],
    'message' => [
      ['id'=>1,'from'=>'some1','to'=>null  ,'when'=>time()-20,'data'=>'hi there'],
      ['id'=>2,'from'=>'finwo','to'=>null  ,'when'=>time()-10,'data'=>'hello back'],
      ['id'=>3,'from'=>'finwo','to'=>'root','when'=>time()   ,'data'=>'this some1 dude is weird'],
    ],
  ],
];
