# chat-app

Basic chat app based on [this prototype](https://gitlab.com/finwo/chat-app-prototype).

Hosted at: [chat.finwo.net](https://chat.finwo.net/)

---

## Project status

* [x] [Prototype build](https://gitlab.com/finwo/chat-app-prototype)
    * [x] Basic project layout
    * [x] Database structure
    * [x] WebSocket protool
    * [x] GUI
    * [x] Storage
* [ ] Deployment automation
    * [x] Google Cloud Project setup
    * [x] Connect CI to container registry
    * [x] Build container image
    * [ ] (optional) run tests
    * [x] Hello World
    * [x] Deploy on GCP cloud runner
* [x] Production version
    * [x] Dockerfile
    * [x] Basic project layout
    * [x] Polling protocol
    * [x] Database structure (minor improvements needed)
    * [x] GUI
    * [x] Private/public distinction
* [ ] Stretch goals
    * [ ] Register new account
    * [ ] Change password
    * [ ] Delete account (gdpr in mind)
    * [ ] Join/leave server messages
    * [ ] Show timestamps for messages
    * [ ] Show command list on login

## Host locally

When run locally, this chat app generates a sqlite database `chatapp.db`

Setup:

```sh
composer install
```

Start locally:

```sh
bin/dev.sh
```
