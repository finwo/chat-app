FROM php:7.2-apache

# Update OS
RUN apt-get update && \
    apt-get install -y --no-install-recommends git zip

# Add mysql support
RUN docker-php-ext-install pdo pdo_mysql mysqli

WORKDIR /app

# Well, the docker build runs as root
ENV COMPOSER_ALLOW_SUPERUSER=1

# Fetch composer
RUN curl --silent --show-error https://getcomposer.org/installer | php

# Setup app
COPY . /app
RUN php composer.phar install

# Use the php built-in server for hosting
# Apache wouldn't honour the PORT variable in a nice way
# If you know a battle-proven server for this, please do tell
CMD php -S 0.0.0.0:$PORT -t /app/public /app/public/index.php
