<?php

use Finwo\ChatApp\Route;
use Finwo\ChatApp\Template;

Route::instance()->respond('GET', '/', function($request, $response, $service, $app, $router) {

  // We need to be logged in here
  if (!isset($app->account)) {
    $response->redirect('/login')->send();
    return;
  }

  return new Template('home', ['account'=>$app->account]);
});
