<?php

use Finwo\ChatApp\Document\Message;
use Finwo\ChatApp\Route;
use Finwo\ChatApp\Template;

Route::instance()->respond('POST', '/message', function($request, $response, $service, $app, $router) {

  // We need to be logged in here
  if (!isset($app->account)) {
    $response->redirect('/login')->send();
    return;
  }

  // Being here = authenticated
  // Verify were not impersonating anyone
  $params       = $request->paramsPost();
  $params->from = $app->account->username;

  // No data = no message
  if (!isset($params->data)) {
    return $response
      ->code(422)
      ->json([
        'success' => false,
        'error'   => 'message-missing-data',
      ]);
  }

  // The server dictates timestamps
  $params->set('when', time());

  // Save the message
  $message = new Message($params);
  $message->save();

  // Return the id
  return $response
    ->code(200)
    ->json([
      'success' => true,
      'id'      => $message->id,
    ]);

});
