<?php

use Finwo\ChatApp\Document\Message;
use Finwo\ChatApp\Route;
use Finwo\ChatApp\Template;

Route::instance()->respond('GET', '/message', function($request, $response, $service, $app, $router) {

  // We need to be logged in here
  if (!isset($app->account)) {
    $response->redirect('/login')->send();
    return;
  }

  // We need a time period
  $params = $request->paramsGet();
  $since  = $params->since;

  // Respond whatever the message document gives
  return $response
    ->code(200)
    ->json([
      'success' => true,
      'data'    => Message::fetch( $app->account->username, $since, 20),
    ]);

});
