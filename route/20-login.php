<?php

use Finwo\ChatApp\Route;
use Finwo\ChatApp\Template;

// Just show the login template
Route::instance()->respond('GET', '/login', function() {
  return new Template('login');
});
