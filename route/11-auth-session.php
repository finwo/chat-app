<?php

use Finwo\ChatApp\Document\Account;
use Finwo\ChatApp\Document\Session;
use Finwo\ChatApp\Route;

// Authenticate by session
Route::instance()->respond(function($request, $response, $service, $app) {
  $cookies = $request->cookies();

  // No session ID = not authenticated here
  if (!isset($cookies->PHPSESSID)) return;

  // Fetch the session
  $app->session = Session::findOne('token', $cookies->PHPSESSID);

  // No username = not authenticated
  if (!isset($app->session->account)) return;

  // Fetch the account & store it
  $app->account = Account::findOne('username', $app->session->account);

});
