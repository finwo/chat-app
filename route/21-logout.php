<?php

use Finwo\ChatApp\Route;
use Finwo\ChatApp\Template;

Route::instance()->respond('GET', '/logout', function($request, $response, $service, $app) {

  // Delete session if it's there
  if (isset($app->session)) {
    $app->session->delete();
  }

  // And return home
  return $response
    ->redirect('/')
    ->send();
});
