<?php

use Finwo\ChatApp\Document\Account;
use Finwo\ChatApp\Document\Session;
use Finwo\ChatApp\Route;

// Authenticate by username/password
Route::instance()->respond('POST', '*', function($request, $response, $service, $app) {
  $params  = $request->paramsPost();

  // Skip request if no credentials are present
  if (!isset($params->username)) return;
  if (!isset($params->password)) return;

  // Fetch the account
  $account = Account::findOne('username', $params->username);

  // Not found = computer says no
  if (is_null($account)) {
    return $response->redirect('/login?error=authenticate')->send();
  }

  // Password mismatch = still no authenticated request
  if (!$account->verifyPassword($params->password)) {
    return $response->redirect('/login?error=authenticate')->send();
  }

  // Execution being here = authenticated
  $app->session = new Session([
    'account' => $account->username,
  ]);

  // Simply store the account
  $app->account = $account;

  // Make the browser remember
  $response->cookie('PHPSESSID', $app->session->token);

  // Redirect
  return $response->redirect($request->uri())->send();
});
