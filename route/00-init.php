<?php

use Finwo\ChatApp\Db;
use Finwo\ChatApp\Route;

// Handle JSON post body
Route::instance()->respond('POST', '*', function($request) {

  // Decode a json body
  $json = json_decode($request->body(),true);
  if (json_last_error() === JSON_ERROR_NONE) {
    foreach ($json as $key => $value) {
      $request->paramsPost()->set($key, $value);
    }
  }

});
