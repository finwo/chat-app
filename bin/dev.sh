#!/usr/bin/env bash

# Ensure bash in approot
[ "$BASH" ] || exec bash "$0" "$@"
cd "$(dirname $0)/.."

# Run php internal server
php -S localhost:8080 -t public public/index.php
